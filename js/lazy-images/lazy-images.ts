/**
 * @class LazyResponsiveImage
 * @author Onstuimig Interactieve Communicatie
 * @version 0.6 - Vanilla JS (Ice Ice Baby)
 */

 class LazyResponsiveImage {

 	// Define private strings to match with
 	// the needed image types
 	private _typeBackground: string = 'background';
 	private _typeImage: string		= 'image';

 	// Public available variables
 	image;
 	imageContainer;
 	screenWidth;
 	imageContainerWatcher;
 	fallbackImage;
 	imageSizes;
 	baseDataAttribute;
 	noLazyClass;
 	imageSource;

 	// Default configuration object
 	config = {
 		loadedClass: 		'is-loaded',
 		type: 		 		'image',
 		noLazyClass: 		'no-lazy',
 		baseDataAttribute:	'data-img-src',
 		viewportOffset: 	250,
 		imageSizes: {
 			481 	:	'mobile',
 			768 	:	'small',
 			1024	:	'tablet'
 		}
 	}

 	constructor(object: any, userConfiguration?: any ) {

 		var extend = function() {
 			var extended = {};

 			for(key in arguments) {
 				var argument = arguments[key];

 				for (prop in argument) {
 					if (Object.prototype.hasOwnProperty.call(argument, prop)) {
 					extended[prop] = argument[prop];
 					}
 				}
 			}

 			return extended;
 		};

 		// Merge the two config objects
 		this.config = extend(this.config, userConfiguration);

 		// Define the imageContainer
 		this.imageContainer = object;

 		// Check if lazy load is needed
 		this.isLazyLoadNeeded();
 	}

 	/**
 	 * @method isLazyLoadNeeded()
 	 * This method checks if we need to lazy load the image,
 	 * if not (when the .no-lazy class is applied to the imageContainer),
 	 * we will collect the image info inmediatly
 	 */
 	isLazyLoadNeeded() {
 		if (this.imageContainer.classList.contains(this.config.noLazyClass)) {
 			this.collectImageInfo();
 		}else {
 			this.createScrollMonitor();
 		}
 	}

 	/**
 	 * @method createScrollMonitor()
 	 * Creates a scrollMonitor instance for the imageContainer
 	 */
 	createScrollMonitor() {
 		this.imageContainerWatcher = scrollMonitor.create(this.imageContainer, this.config.viewportOffset);
 		this.checkEnterViewport();
 	}

 	/**
 	 * @method checkEnterViewport()
 	 * Checks when the imageContainer is in the viewport. When that's the case
 	 * it will either collectImageInfo() or destroyScrollMonitor(), depending
 	 * on if the image is loaded or not
 	 */
 	checkEnterViewport() {
 		this.imageContainerWatcher.enterViewport( () => {
 			if(!this.imageContainer.classList.contains(this.config.loadedClass)) {
 				this.collectImageInfo();
 			}else {
 				this.destroyScrollMonitor();
 			}
 		});
 	}

 	/**
 	 * @method collectImageInfo()
 	 * Grabs the <img> from the imageContainer and does a call to getImageSource()
 	 * to get the responsive image 'attribute'.
 	 */
 	collectImageInfo() {

 		if (this.config.type === this._typeImage ) {

 			this.image 			= this.imageContainer.querySelectorAll('img')[0];
 			this.imageSource 	= this.image.getAttribute( this.getImageSource(this.image) );

 			if (this.imageSource) {
 				this.image.src = this.imageSource;
 				this.handleImageLoad(this.image);
 			}

 		}else if (this.config.type === this._typeBackground ) {

 			this.imageSource 	= this.imageContainer.getAttribute(this.getImageSource(this.imageContainer));

 			if (this.imageSource) {
 				var tempImage 		= new Image();
 				tempImage.src 		= this.imageSource;

 				// pass to image load
 				this.handleImageLoad(tempImage);
 			}


 		}else {
 			console.warn('No allowed type (Allowed types: ['+this._typeImage+' or '+this._typeBackground+']) found in passed configuration object. Aborting...');
 			this.destroyScrollMonitor();
 		}
 	}

 	/**
 	 * @method handleImageLoad()
 	 * checks for image load and adds the imageContainerLoadedClass to the imageContainer
 	 * It also calls destroyScrollMonitor()
 	 */
 	handleImageLoad(image: any) {

 		if (this.config.type === this._typeBackground ) {
 			image.onload = () => {
 				this.setBackgroundImage();
 			}
 		}else {
 			image.onload = () => {
 				this.imageContainer.classList.add(this.config.loadedClass);
 				this.destroyScrollMonitor();
 			}
 		}
 	}

 	/**
 	 * @method setBackgroundImage()
 	 * sets the loaded image to the background-image attribute and loads the image
 	 * from the new created Image() and performs a cache request for this file
 	 */
 	setBackgroundImage() {
 		//this.imageContainer.css('background-image', 'url(' +this.imageSource+ ')');
 		this.imageContainer.style.backgroundImage="url('"+this.imageSource+"')";
 		this.imageContainer.classList.add(this.config.loadedClass);
 		this.destroyScrollMonitor();
 	}

 	/**
 	 * @method destroyScrollMonitor()
 	 * Checks if there's still a scrollMonitor instance set, if so, call
 	 * the destroy() method from the scrollMonitor instance.
 	 */
 	destroyScrollMonitor() {
 		if ( typeof this.imageContainerWatcher != 'undefined' ) {
 			this.imageContainerWatcher.destroy();
 		}
 	}

 	/**
 	 * @method getImageSource()
 	 * checks the windowWidth and calculates the correct responsive image attribute
 	 */
 	getImageSource(element: any) {
 		this.screenWidth 			= this.getWindowWidth();
 		var responsiveDataPostfix 	= '';

 		// Loop trough the image sizes and return the postfix, defined
 		// in the config data attribute

 		for (var key in this.config.imageSizes) {
 			if (this.config.imageSizes.hasOwnProperty(key)) {
 				if(this.screenWidth < parseInt(key) && element.hasAttribute(`${this.config.baseDataAttribute}-${this.config.imageSizes[key]}`)) {
 					responsiveDataPostfix = `-${this.config.imageSizes[key]}`;
 					break;
 				}
 			}
 		}

 		return this.config.baseDataAttribute + responsiveDataPostfix;
 	}

 	/**
 	 * @method getWindowWidth()
 	 * Little helper to return the Window width
 	 */
 	getWindowWidth() {
 		return window.innerWidth;
 	}
 }
