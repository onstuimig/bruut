/*!
* Bruut JS Toolkit
* @namespace: toolkit{func}
* @version 1.1
* @license Copyright 2016 Onstuimig
*/
	let toolkit = {};

/**
 * Adds multiple event listeners to a element
 * el 	- Element: node
 * s	- Events: string
 * fn	- function: function
 */

	toolkit.addEventListeners = (el, s, fn) => {
		s.split(' ').forEach(e => el.addEventListener(e, fn, false));
	}

/**
 * Throttles a function (fires it every $threshold ms)
 * fn			- function: function
 * threshold	- Treshold: number
 * scrope		- scope, defaults to 'this'
 */

	toolkit.throttle = (fn, treshold, scope ? : any) => {
		treshold || (treshold = 250);
		var last,
			deferTimer;
		return function() {
			var context = scope || this;
			var now = +new Date,
				args = arguments;
			if (last && now < last + treshold) {
				// hold on to it
				clearTimeout(deferTimer);
				deferTimer = setTimeout(function() {
					last = now;
					fn.apply(context, args);
				}, treshold);
			} else {
				last = now;
				fn.apply(context, args);
			}
		};
	};

/**
 * Debounces a function (waits a $treshold when done until firing)
 * fn			- function: function
 * threshold	- Treshold: number
 * inmediate	- Fire inmediately? : boolean
 */

	toolkit.debounce = (func, treshold, immediate) => {
		var timeout;
		return function() {
			var context = this,
				args = arguments;
			var later = function() {
				timeout = null;
				if ( !immediate ) {
					func.apply(context, args);
				}
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, treshold || 200);
			if ( callNow ) {
				func.apply(context, args);
			}
		};
	};

/**
 * Climbs up the DOM tree and returns a array of selectors with a given attribute / class / id
 * elem			- the element to start the climb up: element
 * selector		- Attribute that we're looking for: string
 */

	toolkit.getParentSelector = (elem, selector) => {
		var parents = [];
		var firstChar;
		if ( selector ) {
			firstChar = selector.charAt(0);
		}

		// Get matches
		for ( ; elem && elem !== document; elem = elem.parentNode ) {
			if ( selector ) {

				// If selector is a class
				if ( firstChar === '.' ) {
					if ( elem.classList.contains( selector.substr(1) ) ) {
						parents.push( elem );
					}
				}

				// If selector is an ID
				if ( firstChar === '#' ) {
					if ( elem.id === selector.substr(1) ) {
						parents.push( elem );
					}
				}

				// If selector is a data attribute
				if ( firstChar === '[' ) {
					if ( elem.hasAttribute( selector.substr(1, selector.length - 1) ) ) {
						parents.push( elem );
					}
				}

				// If selector is a tag
				if ( elem.tagName.toLowerCase() === selector ) {
					parents.push( elem );
				}

			} else {
				parents.push( elem );
			}

		}

		// Return parents if any exist
		if ( parents.length === 0 ) {
			return null;
		} else {
			return parents;
		}
	});


	(function () {

	  if ( typeof window.CustomEvent === "function" ) return false;

	  function CustomEvent ( event, params ) {
	    params = params || { bubbles: false, cancelable: false, detail: undefined };
	    var evt = document.createEvent( 'CustomEvent' );
	    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
	    return evt;
	   }

	  CustomEvent.prototype = window.Event.prototype;

	  window.CustomEvent = CustomEvent;
	})();