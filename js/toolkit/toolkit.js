var toolkit = {};
toolkit.addEventListeners = function (el, s, fn) {
    s.split(' ').forEach(function (e) { return el.addEventListener(e, fn, false); });
};
toolkit.throttle = function (fn, treshold, scope) {
    treshold || (treshold = 250);
    var last, deferTimer;
    return function () {
        var context = scope || this;
        var now = +new Date, args = arguments;
        if (last && now < last + treshold) {
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, treshold);
        }
        else {
            last = now;
            fn.apply(context, args);
        }
    };
};
toolkit.debounce = function (func, treshold, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) {
                func.apply(context, args);
            }
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, treshold || 200);
        if (callNow) {
            func.apply(context, args);
        }
    };
};
toolkit.getParentSelector = function (elem, selector) {
    var parents = [];
    var firstChar;
    if (selector) {
        firstChar = selector.charAt(0);
    }
    for (; elem && elem !== document; elem = elem.parentNode) {
        if (selector) {
            if (firstChar === '.') {
                if (elem.classList.contains(selector.substr(1))) {
                    parents.push(elem);
                }
            }
            if (firstChar === '#') {
                if (elem.id === selector.substr(1)) {
                    parents.push(elem);
                }
            }
            if (firstChar === '[') {
                if (elem.hasAttribute(selector.substr(1, selector.length - 1))) {
                    parents.push(elem);
                }
            }
            if (elem.tagName.toLowerCase() === selector) {
                parents.push(elem);
            }
        }
        else {
            parents.push(elem);
        }
    }
    if (parents.length === 0) {
        return null;
    }
    else {
        return parents;
    }
};
;
(function () {
    if (typeof window.CustomEvent === "function")
        return false;
    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }
    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();
