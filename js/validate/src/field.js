var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Field = (function (_super) {
    __extends(Field, _super);
    function Field(field, Form) {
        var _this = _super.call(this, field, Form) || this;
        _this.fieldCombines = false;
        if (!_this.field.hasAttribute(_this.fieldNameAttribute))
            _this.fieldNameAttribute = 'name';
        _this.setupGlueGroup();
        _this.getFieldValidationProperties();
        _this.setupValidationByEvent();
        return _this;
    }
    Field.prototype.validate = function (rulesByEvent) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, _b, _i, rule, tmpError, isVisible, event;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        this.fieldIsBusy();
                        _a = [];
                        for (_b in this.fieldConfig)
                            _a.push(_b);
                        _i = 0;
                        _c.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3, 4];
                        rule = _a[_i];
                        if (!this.fieldConfig.hasOwnProperty(rule)) return [3, 3];
                        if (!(rule in this.config.rules)) return [3, 3];
                        if (!(typeof rulesByEvent == 'undefined' || (rulesByEvent.indexOf(rule) >= 0))) return [3, 3];
                        isVisible = !!(this.field.offsetWidth || this.field.offsetHeight || this.field.getClientRects().length);
                        return [4, this.config.rules[rule](this.fieldConfig[rule], this.field.value, this.field)];
                    case 2:
                        tmpError = _c.sent();
                        if (tmpError != true && isVisible || tmpError != true && this.field.hasAttribute(this.config.attributes.fieldValidateOnHidden)) {
                            this.fieldErrors[this.field.getAttribute(this.fieldNameAttribute) + "__" + rule] = tmpError;
                            this.FormInstance.fieldErrors[this.field.getAttribute(this.fieldNameAttribute) + "__" + rule] = tmpError;
                            this.FormInstance.fieldsWithErrors[this.field.getAttribute(this.fieldNameAttribute)] = true;
                        }
                        else {
                            delete this.fieldErrors[this.field.getAttribute(this.fieldNameAttribute) + "__" + rule];
                            delete this.FormInstance.fieldErrors[this.field.getAttribute(this.fieldNameAttribute) + "__" + rule];
                            if (this.FormInstance.fieldsWithErrors[this.field.getAttribute(this.fieldNameAttribute)] == 'undefined') {
                                delete this.FormInstance.fieldsWithErrors[this.field.getAttribute(this.fieldNameAttribute)];
                            }
                            if (!isVisible && !this.field.hasAttribute(this.config.attributes.fieldValidateOnHidden)) {
                                tmpError = true;
                            }
                        }
                        event = new CustomEvent('validated', { detail: { isOk: tmpError } });
                        this.field.dispatchEvent(event);
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4:
                        this.checkFieldErrors();
                        return [2];
                }
            });
        });
    };
    Field.prototype.setupValidationByEvent = function () {
        for (var eventName in this.config.rulesOnEvents) {
            if (this.config.rulesOnEvents.hasOwnProperty(eventName)) {
                (eventName == 'keyup') ? this.setupEventsKeyup(eventName, this.config.rulesOnEvents[eventName]) : this.setupEvents(eventName, this.config.rulesOnEvents[eventName]);
            }
        }
    };
    Field.prototype.setupEvents = function (eventName, rulesByEvent) {
        var _this = this;
        if (this.fieldCombines.length > 0) {
            for (var i = 0; i < this.fieldCombines.length; i++) {
                this.fieldCombines[i].addEventListener(eventName, function () {
                    _this.setValueByGlue();
                    _this.validate(rulesByEvent);
                });
            }
            ;
            this.field.addEventListener(eventName, function (e) {
                _this.setGlueFieldValuesByField();
                _this.validate(rulesByEvent);
            });
        }
        else {
            this.field.addEventListener(eventName, function (e) {
                _this.validate(rulesByEvent);
            });
        }
    };
    Field.prototype.setupEventsKeyup = function (eventName, rulesByEvent) {
        var _this = this;
        var rulesByEvent = rulesByEvent;
        if (this.fieldCombines.length > 0) {
            for (var i = 0; i < this.fieldCombines.length; i++) {
                this.fieldCombines[i].addEventListener(eventName, toolkit.debounce(function () {
                    _this.setValueByGlue();
                    _this.validate(rulesByEvent);
                }, this.config.validationDelay));
            }
            this.field.addEventListener(eventName, toolkit.debounce(function () {
                _this.setGlueFieldValuesByField();
                _this.validate(rulesByEvent);
            }, this.config.validationDelay));
        }
        else {
            this.field.addEventListener(eventName, toolkit.debounce(function () {
                _this.validate(rulesByEvent);
            }, this.config.validationDelay));
        }
    };
    Field.prototype.getFieldValidationProperties = function () {
        var props = this.field.getAttribute(this.config.attributes.fieldValidationSettings).replace(/ /g, '').split(',');
        for (var i = 0; i < props.length; i++) {
            var tmprule = props[i].split(/:(.+)/);
            this.fieldConfig[tmprule[0]] = tmprule[1] || null;
        }
    };
    Field.prototype.setupGlueGroup = function () {
        if (this.field.hasAttribute(this.config.attributes.fieldCombineGlue)) {
            this.fieldParent = this.config.field.parentGroupClass;
            this.fieldCombines = this.FormInstance.form.querySelectorAll("[" + this.config.attributes.fieldCombine + "=\"" + this.field.getAttribute(this.config.attributes.fieldCombineGlue) + "\"]");
            this.fieldNameAttribute = this.config.attributes.fieldCombine;
            this.isCombineField = true;
            this.fieldNameAttribute = this.config.attributes.fieldCombineGlue;
        }
        this.getParentNodes();
    };
    Field.prototype.setValueByGlue = function () {
        var glueValues = [];
        for (var i = 0; i < this.fieldCombines.length; i++) {
            glueValues[i] = this.fieldCombines[i].value;
        }
        this.field.value = glueValues.join(this.field.getAttribute(this.config.attributes.fieldGlueCharacter)).trim();
    };
    Field.prototype.setGlueFieldValuesByField = function () {
        var glueValues = this.field.value.split(this.field.getAttribute(this.config.attributes.fieldGlueCharacter), this.fieldCombines.length);
        for (var i = 0; i < this.fieldCombines.length; i++) {
            this.fieldCombines[i].value = (typeof (glueValues[i]) != 'undefined') ? glueValues[i] : '';
        }
    };
    return Field;
}(FieldBase));
