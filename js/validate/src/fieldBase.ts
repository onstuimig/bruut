class FieldBase {

	fieldErrors; field; fieldConfig: Object; config; FormInstance; fieldParent; fieldNameAttribute; isCombineField; isGroupField; parents;

	constructor(field, Form) {
		this.field 				= field;
		this.fieldConfig 		= {};
		this.fieldErrors 		= {};
		this.FormInstance		= Form;
		this.config 			= Form.config;
		this.fieldParent		= this.config.field.parentClass;
		this.fieldNameAttribute	= this.config.attributes.fieldName;
		this.isGroupField		= false;
		this.isCombineField 	= false;
		this.parents			= false;
	}

	getParentNodes() {
		if(this.isGroupField) {
			this.parents = this.field;

		}else if (this.isCombineField) {

			this.parents = this.FormInstance.form.querySelectorAll(`[${this.config.attributes.fieldCombine}="${this.field.getAttribute( this.fieldNameAttribute )}"]`)[0];

			this.parents = toolkit.getParentSelector(this.parents, `${this.config.field.parentGroupClass}`)[0];

		}else {
			// Standard field
			this.parents = this.FormInstance.form.querySelectorAll(`[${this.fieldNameAttribute}="${this.field.getAttribute( this.fieldNameAttribute )}"]`)[0];

			this.parents = toolkit.getParentSelector(this.parents, `${this.config.field.parentClass}`)[0];
		}
	}

	checkFieldErrors() {
		(Object.keys(this.fieldErrors).length == 0) ? this.fieldIsOk() : this.fieldHasErrors();
		this.fieldIsTouched();
	}

	fieldIsTouched() {
		// Remove Busy class
		this.parents.classList.remove(this.config.classes.busy);

		this.parents.classList.add(this.config.classes.touched);
	}

	fieldIsBusy() {
		this.parents.classList.add(this.config.classes.busy);
	}

	fieldHasErrors() {

		this.parents.classList.add(this.config.classes.error);
		this.parents.classList.remove(this.config.classes.success);

		var messageBoxes = this.FormInstance.form.querySelectorAll(`[${this.config.attributes.fieldMessage}*=",${this.field.getAttribute( this.fieldNameAttribute )},"]`);


		for(let i = 0, len = messageBoxes.length; i < len; ++i) {
			messageBoxes[i].classList.remove(this.config.classes.success);
			messageBoxes[i].classList.add(this.config.classes.error);

			var errorText = this.FormInstance.form.querySelector(`[${this.config.attributes.fieldMessageErrorHtml}="${this.field.getAttribute( this.fieldNameAttribute)}"]`);
			var errorExists = messageBoxes[i].querySelector(`[${this.config.attributes.fieldMessageErrorHtml}="${this.field.getAttribute( this.fieldNameAttribute)}"]`);

			var successExist = messageBoxes[i].querySelector(`[${this.config.attributes.fieldMessageOkHtml}="${this.field.getAttribute( this.fieldNameAttribute)}"]`);
			if (successExist != null)
				messageBoxes[i].removeChild(successExist);

			if( errorText !== null && errorExists == null) {
				errorText = errorText.cloneNode(true);
				if (errorText.innerHTML != '') {
					messageBoxes[i].appendChild(errorText);
					errorText.classList.add(this.config.classes.error);
				}
			}
		}
	}

	fieldIsOk() {

		this.parents.classList.remove(this.config.classes.error);
		this.parents.classList.add(this.config.classes.success);

		var messageBoxes = this.FormInstance.form.querySelectorAll(`[${this.config.attributes.fieldMessage}*=",${this.field.getAttribute( this.fieldNameAttribute )},"]`);

		for(let i = 0, len = messageBoxes.length; i < len; ++i) {
			messageBoxes[i].classList.remove(this.config.classes.error);
			messageBoxes[i].classList.add(this.config.classes.success);

			var successText = this.FormInstance.form.querySelector(`[${this.config.attributes.fieldMessageOkHtml}="${this.field.getAttribute( this.fieldNameAttribute)}"]`);
			var successExists = messageBoxes[i].querySelector(`[${this.config.attributes.fieldMessageOkHtml}="${this.field.getAttribute( this.fieldNameAttribute)}"]`);

			var errorExist = messageBoxes[i].querySelector(`[${this.config.attributes.fieldMessageErrorHtml}="${this.field.getAttribute( this.fieldNameAttribute)}"]`)
			if (errorExist != null)
				messageBoxes[i].removeChild(errorExist);

			if( successText !== null && successExists == null) {
				successText = successText.cloneNode(true);
				if (successText.innerHTML != '') {
					messageBoxes[i].appendChild(successText);
					successText.classList.add(this.config.classes.success);
				}
			}
		}
	}
}
