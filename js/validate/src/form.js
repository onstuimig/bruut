var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var FormValidate = (function () {
    function FormValidate(form, userConfiguration) {
        this.config = {
            validationDelay: 150,
            handleSubmitButton: true,
            classes: {
                error: 'has-errors',
                success: 'is-ok',
                busy: 'is-validating',
                touched: 'is-touched',
            },
            rules: {
                'required': function (length, fieldValue, field) {
                    if (typeof field.type !== 'undefined' && field.type === 'file') {
                        return (field.files.length > 0) ? true : false;
                    }
                    return (fieldValue.trim() == '' ? false : true);
                },
                'min-length': function (length, fieldValue) {
                    if (fieldValue.trim() == '')
                        return true;
                    return (fieldValue.length >= length);
                },
                'max-length': function (length, fieldValue) {
                    return (fieldValue.length <= length);
                },
                'characters': function (type, fieldValue) {
                    if (type === 'alpha') {
                        var pattern = /^[a-zA-Z]*$/;
                        return (pattern.test(fieldValue));
                    }
                    else if (type === 'alphaspace') {
                        var pattern = /^[a-zA-Z\s]*$/;
                        return (pattern.test(fieldValue));
                    }
                    else if (type === 'alphaspacedash') {
                        var pattern = /^[a-zA-Z\s-]*$/;
                        return (pattern.test(fieldValue));
                    }
                    else if (type === 'numeric') {
                        if (fieldValue.length == 0)
                            return true;
                        var mixed_var = fieldValue;
                        var whitespace = ' \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000';
                        return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) ===
                            -1)) && mixed_var !== '' && !isNaN(Number(mixed_var));
                    }
                    else {
                        console.warn('characters property on field can only contain "alpha|numeric|alphaspace|alphaspacedash"');
                    }
                },
                'email': function (data, fieldValue) {
                    if (fieldValue.length == 0)
                        return true;
                    var pattern = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
                    return (pattern.test(fieldValue));
                },
                'postcode': function (data, fieldValue) {
                    if (fieldValue.length == 0)
                        return true;
                    var pattern = /^[1-9][0-9]{3}[\s]?(?!SS|SA|SD)[A-Z]{2}$/i;
                    return !!fieldValue.match(pattern);
                },
                'date': function (data, fieldValue) {
                    if (fieldValue.length == 0)
                        return true;
                    var pattern = /^(?:(?:31(\/|-|\.|\s)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.|\s)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.|\s)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.|\s)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
                    return !!fieldValue.match(pattern);
                }
            },
            rulesOnEvents: {
                'keyup': ['min-length', 'max-length', 'email', 'characters', 'required', 'postcode', 'date', 'match-password'],
                'change': ['min-checked', 'max-checked', 'match-password', 'required']
            },
            field: {
                parentClass: '.form-element',
                parentGroupClass: '.form-group',
            },
            attributePrefix: 'v',
            attributes: {
                fieldName: 'name',
                fieldValidationSettings: 'props',
                fieldGroup: 'group',
                fieldGroupChild: 'group-child',
                fieldCombine: 'combine',
                fieldCombineGlue: 'glue-result',
                fieldGlueCharacter: 'glue-character',
                fieldMessage: 'message-for',
                fieldMessageErrorHtml: 'error-for',
                fieldMessageOkHtml: 'ok-for',
                fieldValidateOnHidden: 'on-hidden'
            }
        };
        this.form = (form.nodeType) ? form : form[0];
        this.formFields = [];
        this.formGroups = [];
        this.fieldErrors = {};
        this.isValid = false;
        this.fieldsWithErrors = [];
        this.config = Object.deepExtend(this.config, userConfiguration);
        for (var key in this.config.attributes) {
            if (this.config.attributes.hasOwnProperty(key)) {
                this.config.attributes[key] = this.config.attributePrefix + "-" + this.config.attributes[key];
            }
        }
        this.normalizeMessageReceivers();
        this.update();
        this.handleSubmit();
    }
    FormValidate.prototype.normalizeMessageReceivers = function () {
        var messageBoxes = this.form.querySelectorAll("[" + this.config.attributes.fieldMessage + "]");
        for (var i = 0, len = messageBoxes.length; i < len; ++i) {
            var attr = messageBoxes[i].getAttribute("" + this.config.attributes.fieldMessage);
            attr = attr.replace(/, /g, ',');
            messageBoxes[i].setAttribute("" + this.config.attributes.fieldMessage, "," + attr + ",");
        }
    };
    FormValidate.prototype.update = function () {
        var _fields = this.form.querySelectorAll("[" + this.config.attributes.fieldValidationSettings + "]");
        var _fieldGroups = this.form.querySelectorAll("[" + this.config.attributes.fieldGroup + "]");
        for (var i = 0; i < _fields.length; i++) {
            this.formFields[i] = new Field(_fields[i], this);
        }
        for (var i = 0; i < _fieldGroups.length; i++) {
            this.formGroups[i] = new FieldGroup(_fieldGroups[i], this);
        }
    };
    FormValidate.prototype.handleSubmit = function () {
        var _this = this;
        if (this.config.handleSubmitButton === true) {
            this.form.addEventListener('submit', function (e) {
                e.preventDefault();
                _this.validate(function () {
                    (_this.isValid) ? _this.formIsOk() : _this.formHasErrors();
                    if (_this.isValid)
                        _this.form.submit();
                });
            });
        }
    };
    FormValidate.prototype.formHasErrors = function () {
        this.form.classList.add(this.config.classes.error);
        this.form.classList.remove(this.config.classes.success);
        var messageBoxes = this.form.querySelectorAll("[" + this.config.attributes.fieldMessage + "*=\",form,\"]");
        for (var i = 0, len = messageBoxes.length; i < len; ++i) {
            messageBoxes[i].classList.add(this.config.classes.error);
            messageBoxes[i].classList.remove(this.config.classes.success);
            var errorText = this.form.querySelector("[" + this.config.attributes.fieldMessageErrorHtml + "=\"form\"]");
            if (errorText !== null) {
                messageBoxes[i].innerHTML = errorText.innerHTML;
            }
        }
    };
    FormValidate.prototype.formIsOk = function () {
        this.form.classList.remove(this.config.classes.error);
        this.form.classList.add(this.config.classes.success);
        var messageBoxes = this.form.querySelectorAll("[" + this.config.attributes.fieldMessage + "*=\",form,\"]");
        for (var i = 0, len = messageBoxes.length; i < len; ++i) {
            messageBoxes[i].classList.remove(this.config.classes.error);
            messageBoxes[i].classList.add(this.config.classes.success);
            var successText = this.form.querySelector("[" + this.config.attributes.fieldMessageOkHtml + "=\"form\"]");
            if (successText !== null) {
                messageBoxes[i].innerHTML = successText.innerHTML;
            }
        }
    };
    FormValidate.prototype.addValidator = function (obj) {
        this.config.rules[obj['validatorName']] = obj.logic;
        if (typeof this.config.rulesOnEvents[obj['onEvent']] == 'undefined')
            this.config.rulesOnEvents[obj['onEvent']] = new Array();
        this.config.rulesOnEvents[obj['onEvent']].push(obj['validatorName']);
    };
    FormValidate.prototype.validate = function (callback) {
        return __awaiter(this, void 0, void 0, function () {
            var i, len, i, len;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.form.classList.add(this.config.classes.busy);
                        i = 0, len = this.formFields.length;
                        _a.label = 1;
                    case 1:
                        if (!(i < len)) return [3, 4];
                        return [4, this.formFields[i].validate()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        ++i;
                        return [3, 1];
                    case 4:
                        i = 0, len = this.formGroups.length;
                        _a.label = 5;
                    case 5:
                        if (!(i < len)) return [3, 8];
                        return [4, this.formGroups[i].validate()];
                    case 6:
                        _a.sent();
                        _a.label = 7;
                    case 7:
                        ++i;
                        return [3, 5];
                    case 8:
                        this.isValid = (Object.keys(this.fieldErrors).length == 0) ? true : false;
                        this.form.classList.remove(this.config.classes.busy);
                        if (typeof callback == 'function') {
                            callback(this.isValid);
                        }
                        return [2, this.isValid];
                }
            });
        });
    };
    return FormValidate;
}());
Object.deepExtend = function (destination, source) {
    for (var property in source) {
        if (source[property] && source[property].constructor &&
            source[property].constructor === Object) {
            destination[property] = destination[property] || {};
            arguments.callee(destination[property], source[property]);
        }
        else {
            destination[property] = source[property];
        }
    }
    return destination;
};
