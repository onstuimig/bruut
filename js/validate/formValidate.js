/*!
* formValidate.js
* @version: 1.0.2
* @license: Copyright 2016 Onstuimig
* @dependency: bruut toolkit ( > 1.1 )
*/

// @codekit-append "vendor/es6-promise.auto.min.js";

// @codekit-append "src/form.js";
// @codekit-append "src/fieldBase.js";
// @codekit-append "src/field.js";
// @codekit-append "src/fieldGroup.js";
