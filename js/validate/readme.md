This plugin adds the functionality to validate input fields on the front-end, before submitting a form.

## 1. Install the plugin

Include the formValidate.js plugin (included in Bruut 3.x) and create a new `instance` of `FormValidate`:

	var form       = document.getElementById('myForm');
	var validator  = new FormValidate( form );

## 2. Basic usage

Let’s start with the most simple example:

	<form id="myForm" novalidate autocomplete="off">
		<input type="text" v-name="myInput" v-props="required, max-length:3">
	</form>

This field will be checked on `keyup` to check if there’s a value at all (`required`) in `v-props` and if it has a max-length of 4. The `v-name` attribute can be omitted, but the `name` attribute has to be unique for every input field or input group.

**Takeaway** : Use a unique `v-name` or the `name` attribute to add a field to the validator plugin. Use the `v-props` attribute to add checks to this field, seperated by a comma.

### Available checks

### For input (text) fields:

- **required** | Checks if any value exists
- **min-length:(int)** | Checks a minimum length
- **max-length:(int)** |  Checks a maximun length
- **characters:alpha** | Checks if value only contains alpha characters. Pattern: `/^[a-zA-Z]*$/`.
- **characters:alphaspace** | Checks if value only contains alpha characters or spaces. Pattern: `/^[a-zA-Z\s]*$/`.
- **characters:alphaspacedash** | Checks if value only contains alpha characters or spaces or dashes. Pattern: `/^[a-zA-Z\s-]*$/`.
- **characters:numeric** | Checks if value only contains numeric characters. Pattern: `/^[a-zA-Z\s-]*$/`
- **email** | Checks for a proper email address.
- **postcode** | Checks for a ‘dutch style’ ZIP code (0000 AA)
- **date** | Checks for a date in the following pattern: DD-MM-YYYY / DD-MM-YY

### For checkbox / radio groups (requires a `Field Group`):

- **min-checked:(int)** | Checks a minimum checked amount of items
- **max-checked:(int)** |  Checks a maximun checked amount of items and stops the user from checking any more items once exceeding the `max-checked` treshold.

### Special checks (requires a `Field Group`):

- **match-password** | Matches two inputs for the exact same value

### Select field:
- **required** | Checks if the selected option isn’t the default or empty option. Requires the following structure:

```
<div class=“form-element”>
	<select name="mySelect" v-props="required">
		<option value=“”>Choose…</option>
		<option value=“myValue”>My option</option>
	</select>
</div>
```

The plugin will check if the `select` has a other option selected than the option with no value (`length 0`). If so, the select returns `true` for validated. Be sure to add values to the other options.

> The plugin handles only single selects (`select-one`) at the moment.


## 3. Specials (Field Groups, Combine Fields)

### Field Group

The field group is used to group fields (inputs and radio / checkbox) together and add validation to the group.

*Example: Matching passwords*

A basic example for matching passwords, using the `match-password` check:

	<div class="form-group" v-group="group:match, match-password">
		<div class="form-element">
			<input type="password" v-group-child="match" v-name="password1" v-props="required, min-length:2">
		</div>
		<div class="form-element">
			<input type="password" v-group-child="match" v-name="password2" v-props="required, min-length:2">
		</div>
	</div>

We created a wrapper with the `v-group` attribute. Define a `group:` name and add the `match-password` check to this group as well (hence the comma!)

In the wrapper, we have two password inputs. They will have the `v-group-child` attribute with the group name of `v-group`. If you want full control, you can validate the individual password inputs as well, using `v-name` and `v-props`. This is useful for custom error / success messages for the individual inputs and the group.

*Example: Checkboxes*
Another basic example, to validate a group of checkboxes:

	<div class="form-element" v-group="group:myCheckboxes, min-checked:1, max-checked: 2">
		<input type="checkbox" v-group-child="myCheckboxes">
		<input type="checkbox" v-group-child="myCheckboxes">
		<input type="checkbox" v-group-child="myCheckboxes">
		<input type="checkbox" v-group-child="myCheckboxes">
	</div>

Again, a wrapper is created for the group, with the name ‘myCheckboxes`. There must be between 1 and 2 items checked to pass the validation for this group.

### Combine Field

Sometimes, you want to combine values from different inputs to check if the combined value matches a certain pattern. You can do so with `Combine Fields`, where you can ‘glue’ various input values to a other input field. A basic example:

	<div class="form-element">
		<input type="text" v-name="birthDateDay" v-combine="birthDate" v-props="characters:numeric, max-length:2">
	</div>
	<div class="form-element">
		<input type="text" v-name="birthDateMonth" v-combine="birthDate" v-props="characters:numeric, max-length:2">
	</div>
	<div class="form-element">
		<input type="text" v-name="birthDateYear" v-combine="birthDate" v-props="characters:numeric, min-length:4, max-length:4">
	</div>

As you can see, we use a `v-combine` property in the fields. With this property, we tell a other input with the same name in the `v-glue-result` property to check the value against a `validator`:

	<div class="form-element">
		<input type="text" v-glue-result="birthDate" v-glue-character="-" v-props="date">
	</div>

Here, we use the `v-glue-result` attribute to fetch the `v-combine` fields with the same name. The `v-glue-character` attribute allows you to add a character between the values of each input. In our example, a completely filled in field group will result in this value in the `v-glue-result` input: 00-00-0000. We validate this value with the (Dutch) `date` check.

## 4. Submitting / Validating the form

Each `Field` that has a `v-name` and each `Field Group` that has the `v-group="group:(name)` attribute set will be validated on keyup or change. If you add a `submit` button to the form, it will automatically validate before submitting. If there are still errors, the submit will be prevented. If you want to bypass the standard behaviour, set the option `handleSubmitButton` to `false` and write your own logic, like so:

	var button = document.getElementById('mySubmitButton');

	button.addEventListener('click, function(e) {
		e.preventDefault();

		if(validator.validate()) {
			validator.form.submit;
		}
	});

A few things to note: We’re calling the `validate()` method, that will return a boolean, stating the validation process. If there are no errors, we submit the real form, by calling the `form` property on the `FormValidate` instance.

## 5. Adding error and success messages

Included in the plugin is a way to show success and error messages per `Field` or `Field Group` (or even both :) ). The basic setup requires a element with the `v-errors` attribute attached:

	<ul class="errors hidden" v-errors>
		<li v-error-for=“myField”>Geen geldig e-mailadres</li>
		<li v-ok-for="myField"></li>
	</ul>

The `v-error-for` and `v-ok-for` allows you to come up with custom messages (or even custom HTML, if you feel fancy). Add the name of your `Field` : (`v-name=“myField”`) or `Field Group` : (`v-group="group:myGroupName`).

The content will be injected in any element (inside the form) with the `v-message-for` attribute:

	<div v-message-for="myField"></div>

If you would like to add a message for the entire form, you can use the reserved `form` as `v-error-for`, `v-ok-for` and `v-message-for`. Don’t use this keyword for other fields.


## 6. Extending FormValidate with your own validators

You can add your own validator logic to the plugin (only on single input fields) for now. Let’s make a validator for a `hex` value which will validate if the value in a field is a hex value, starting with a # and 3 or 6 characters:

	validator = new Formvalidate( form );

	validator.addValidator({
		validatorName: 	'hexValidator',
		onEvent: 		'keyup',
		logic: 	function(dataFromTemplate, fieldValue, field) {
			var pattern = /^#?([a-f0-9]{6}|[a-f0-9]{3})$/;
			return (pattern.test(fieldValue));
		}
	});

the `validatorName` is used in the `v-props` attribute of your field. `onEvent` will let you determine when to validate (keyup | change). The `logic` is a function that has 3 parameters: `dataFromTemplate`, `fieldValue` and `field`. You **must** return a *true* or *false* value for your custom validator to work properly.

- `dataFromTemplate` | If you use for example `v-props=“hex:6”`, this parameter will return 6. If you don’t work with a value behind the `validatorName`, this will return `null`.

- `fieldValue` | This will return the current value in the field on each iteration of the event in `onEvent` (each keyup, or each change).

- ‘field` | The DOM representation of the field.

You can use this new validator in your markup as follows:

	<input type="text" v-name="myHexField" v-props="required, hexValidator">

