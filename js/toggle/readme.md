This plugin adds the functionality to toggle classes to elements, as well as remembering the current state in the LocalStorage.

### Usage

Include the toggle.js plugin (included in Bruut 3.x) and create a new `instance` of Toggle:

	document.addEventListener("DOMContentLoaded", function () {
	    var myToggles = new Toggle();
	});

You can pass some options in the instance as well:

	var myToggles = new Toggle({
		attribute: 'data-toggle',
		storageKeyPrefix: 'data-toggle__',
		defaultToggleClass: 'is-active',
		elementKey: 'who',
		classKey: 'class',
		storageKey: 'save',
		callbackKey: 'callback',
	});

- **Attribute** | The data-attribute that triggers the `Toggle`. Default: `data-toggle`
- **storageKeyPrefix** | The name of the key in `LocalStorage` when using the `save` option. Default: `data-toggle__`
- **defaultToggleClass** | The name of the default class to toggle (when not using the `class` option. Default: `is-active`
- **elementKey** | The name of the selector getting option. Default: `who`
- **classKey**: | The name of the class getting option. Default: `class`
- **storageKey**: | The name of the save to LocalStorage getting option. Default: `save`
- **callbackKey**: | The name of the callback getting option. Default: `callback`

### Barebones example

	<a href="#" data-toggle>Click me</a>

When used like this, the plugin will:
- Toggle the class, set in the configuration options object under `	defaultToggleClass` to the element with the data attribute. The default class is `is-active`.

### All options in the data attributes

	<a href="#" data-toggle='{"who" : "", "class" : "", "save" : ""}'>Click me</a>

> Please note: If you use this expanded version of data-toggle, that you must have a valid jSON encoded string.

- **who** | Optional (default: `this`). The ‘who’ property can take many different selectors:
    - **this** - will get the <a> element with `data-toggle` on it
    - **parent** - Will get the parentNode
    - **parent .selector** / **parent #selector** - Will get the first parent element that matches this query
    - **parent li** / **parent div** - Will get the first parent element that matches this html element query
    - **.selector** - Will get all selectors that match this query

- **Class** | Optional (default: `is-active`)
*The `class` property takes a `string` as class name that will be toggled*

- **Save** | Optional (default: none)
*The `save` property takes a `string` that will be saved in the browser’s LocalStorage. It will be removed from `LocalStorage` if the toggle is back to it’s `default` state*.

- **Callback** | Optional (default: none)
*The `callback` property takes a `string` that will be used to call a function with that name when the toggle is clicked. The instance of `Toggle` will be send as a parameter.

### Examples

	<a href="#" data-toggle='{"who" : "parent .gridComponent"}'>Click me</a>
This will toggle the class of the first parent of the `<a>` element with the default class (`is-active`).

	<a href="#" data-toggle='{"class" : "myNewClass"}'>Click me</a>
This will toggle the class `myNewClass` of the `<a>` element

	<a href="#" data-toggle='{"save" : "isElementClicked"}'>Click me</a>
This will save the current state of the toggled element in the `LocalStorage` with the key `isElementClicked`. The value is the value of the `data-attribute`, for easy reference to the plugin.

	<a href="#" data-toggle=‘{“who” : “.mySelector”, ”class" : "myNewClass”, “save” : “areSelectorsClicked”}’>Click me</a>
This will toggle the class `myNewClass` on all elements with the class `mySelector`. It also saves the state in the `LocalStorage` with key `areSelectorsClicked`.

	<a href="#" data-toggle=‘{“who” : “.mySelector”, ”class" : "myNewClass”, “callback” : “myFunction”}’>Click me</a>
This will toggle the class `myNewClass` on all elements with the class `mySelector`. It will also call the function `myFunction()`, if you have created that function.
