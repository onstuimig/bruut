/*!
* Toggle.js
* @version: 1.0.3
* @license: Copyright 2016 Onstuimig
* @dependency: bruut toolkit ( > 1.1 )
* @creators: Tim | Gerard
*/
var Toggle = (function () {
    function Toggle(config) {
        var _this = this;
        this.props = {
            attribute: 'data-toggle',
            storageKeyPrefix: 'data-toggle__',
            defaultToggleClass: 'is-active',
            elementKey: 'who',
            classKey: 'class',
            storageKey: 'save',
            callbackKey: 'callback',
        };
        var extend = function () {
            var extended = {};
            for (key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        this.props = extend(this.props, config);
        document.body.addEventListener('click', function (e) {
            var tempTarget = e.target;
            if (typeof e.detail != 'undefined' && typeof e.detail.target !== 'undefined') {
                tempTarget = e.detail.target;
            }
            for (var target = tempTarget; target && target != window && target != document; target = target.parentNode) {
                if (target.hasAttribute("" + _this.props.attribute)) {
                    if ((target.type && target.type !== 'checkbox' && target.type !== 'radio') || !target.type) {
                        e.preventDefault();
                    }
                    _this.handleClick(target, e);
                    break;
                }
            }
        });
        this.getKeysFromLocalStorage();
    }
    Toggle.prototype.handleClick = function (target, e) {
        this.clickedElement = (e.target.tagName !== 'BODY') ? target : e.detail.target;
        this.getProperties();
        this.getCallback();
    };
    Toggle.prototype.getProperties = function () {
        this.classIsSet = false;
        try {
            this.data = JSON.parse(this.clickedElement.getAttribute(this.props.attribute));
        }
        catch (e) {
            this.data = this.clickedElement.getAttribute(this.props.attribute);
        }
        this.getNodes();
    };
    Toggle.prototype.getNodes = function () {
        if (typeof this.data == 'string') {
            this.getTargetElement(this.data);
        }
        else {
            (typeof this.data[this.props.elementKey] !== 'undefined') ? this.getTargetElement(this.data[this.props.elementKey]) : this.targetElement = this.clickedElement;
        }
        this.getClass();
    };
    Toggle.prototype.getClass = function () {
        if (typeof this.data == 'string' || typeof this.data[this.props.classKey] == 'undefined') {
            this.classToToggle = this.props.defaultToggleClass;
        }
        else {
            this.classToToggle = this.data[this.props.classKey];
        }
        this.toggleClass();
    };
    Toggle.prototype.getCallback = function () {
        if (typeof this.data !== 'string' && typeof this.data[this.props.callbackKey] !== 'undefined') {
            var param = [this];
            var fn = window[this.data[this.props.callbackKey]];
            if (typeof fn === "function") {
                fn.apply(null, param);
            }
        }
    };
    Toggle.prototype.toggleClass = function () {
        if (this.targetElement == this.clickedElement || typeof this.targetElement.length == 'undefined') {
            this.targetElement.classList.toggle(this.classToToggle);
            if (this.targetElement.classList.contains(this.classToToggle)) {
                this.classIsSet = true;
            }
        }
        else {
            for (var i = 0; i < this.targetElement.length; i++) {
                this.targetElement[i].classList.toggle(this.classToToggle);
                if (this.targetElement[i].classList.contains(this.classToToggle)) {
                    this.classIsSet = true;
                }
            }
        }
        this.checkForStorage();
    };
    Toggle.prototype.checkForStorage = function () {
        if (typeof this.data[this.props.storageKey] !== 'undefined') {
            if (this.classIsSet) {
                localStorage.setItem("" + this.props.storageKeyPrefix + this.data[this.props.storageKey], this.clickedElement.getAttribute(this.props.attribute));
            }
            else {
                localStorage.removeItem("" + this.props.storageKeyPrefix + this.data[this.props.storageKey]);
            }
        }
    };
    Toggle.prototype.getKeysFromLocalStorage = function () {
        for (var key in localStorage) {
            if (key.indexOf(this.props.storageKeyPrefix) !== -1) {
                var elements = document.querySelectorAll("[" + this.props.attribute + "='" + localStorage.getItem(key) + "']");
                for (var i = 0; i < elements.length; i++) {
                    var target = {
                        target: elements[i]
                    };
                    document.body.dispatchEvent(new CustomEvent('click', { 'detail': target }));
                }
            }
        }
    };
    Toggle.prototype.getTargetElement = function (val) {
        if (val == '' || val == 'this') {
            this.targetElement = this.clickedElement;
        }
        else if (val != '' && val != 'this') {
            if (val == 'parent') {
                this.targetElement = this.clickedElement.parentNode;
            }
            else if (val.indexOf("parent ") !== -1) {
                try {
                    this.targetElement = toolkit.getParentSelector(this.clickedElement, val.slice(7));
                    if (this.targetElement.length > 0)
                        this.targetElement = this.targetElement[0];
                }
                catch (e) {
                    console.warn("[data-toggle] : Can't find parent of [." + this.clickedElement.classList + "] with attribute: [" + val + "]. Aborting...");
                }
            }
            else {
                this.targetElement = document.querySelectorAll(val);
            }
        }
    };
    return Toggle;
}());
