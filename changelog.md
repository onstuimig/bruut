#Bruut Framework -  Changelog

## 3.2
Required minimum version of Bruut Project Starter: **3.2.0**

_Changes_:

* `$output-flex` helper (to output flex selectors)
* **DELETED**: flex alignments properties, since this will be at sass/components-bruut/_flex.scss. Don't update your project to Bruut 3.2 if your Project Starter version is below **3.2.0**!.
* Added changelog.md for info about updates
* JS - Updated readme for validator (missed an H2 somewhere)
